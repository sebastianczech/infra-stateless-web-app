resource "aws_vpc" "vpc_web_app" {
  cidr_block       = var.vpc_cird_block
  instance_tenancy = "default"

  tags = {
    Name = "vpc_web_app"
  }
}

resource "aws_subnet" "public_subnet1" {
  vpc_id            = aws_vpc.vpc_web_app.id
  cidr_block        = var.subnet_cird_block_public1
  availability_zone = var.az1

  tags = {
    Name = "public_subnet1"
  }
}

resource "aws_subnet" "public_subnet2" {
  vpc_id            = aws_vpc.vpc_web_app.id
  cidr_block        = var.subnet_cird_block_public2
  availability_zone = var.az2

  tags = {
    Name = "public_subnet2"
  }
}

resource "aws_subnet" "private_subnet1" {
  vpc_id            = aws_vpc.vpc_web_app.id
  cidr_block        = var.subnet_cird_block_private1
  availability_zone = var.az1

  tags = {
    Name = "private_subnet1"
  }
}

resource "aws_subnet" "private_subnet2" {
  vpc_id            = aws_vpc.vpc_web_app.id
  cidr_block        = var.subnet_cird_block_private2
  availability_zone = var.az2

  tags = {
    Name = "private_subnet2"
  }
}

resource "aws_internet_gateway" "ig_web_app" {
  vpc_id = aws_vpc.vpc_web_app.id

  tags = {
    Name = "ig_web_app"
  }
}

resource "aws_eip" "eip_nat1" {
  vpc = true
}


resource "aws_eip" "eip_nat2" {
  vpc = true
}

resource "aws_nat_gateway" "gw_nat1" {
  allocation_id = aws_eip.eip_nat1.id
  subnet_id     = aws_subnet.public_subnet1.id

  tags = {
    Name = "gw_nat1"
  }

  depends_on = [aws_internet_gateway.ig_web_app]
}

resource "aws_nat_gateway" "gw_nat2" {
  allocation_id = aws_eip.eip_nat2.id
  subnet_id     = aws_subnet.public_subnet2.id

  tags = {
    Name = "gw_nat2"
  }

  depends_on = [aws_internet_gateway.ig_web_app]
}

resource "aws_route_table" "route_table_public1" {
  vpc_id = aws_vpc.vpc_web_app.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.ig_web_app.id
  }

  tags = {
    Name = "route_table_public1"
  }
}

resource "aws_route_table" "route_table_public2" {
  vpc_id = aws_vpc.vpc_web_app.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.ig_web_app.id
  }

  tags = {
    Name = "route_table_public2"
  }
}

resource "aws_route_table" "route_table_private1" {
  vpc_id = aws_vpc.vpc_web_app.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.gw_nat1.id
  }

  tags = {
    Name = "route_table_private1"
  }
}

resource "aws_route_table" "route_table_private2" {
  vpc_id = aws_vpc.vpc_web_app.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.gw_nat2.id
  }

  tags = {
    Name = "route_table_private2"
  }
}

resource "aws_route_table_association" "public_subnet1_router_table1" {
  subnet_id      = aws_subnet.public_subnet1.id
  route_table_id = aws_route_table.route_table_public1.id
}

resource "aws_route_table_association" "public_subnet2_router_table2" {
  subnet_id      = aws_subnet.public_subnet2.id
  route_table_id = aws_route_table.route_table_public2.id
}

resource "aws_route_table_association" "private_subnet1_router_table1" {
  subnet_id      = aws_subnet.private_subnet1.id
  route_table_id = aws_route_table.route_table_private1.id
}

resource "aws_route_table_association" "private_subnet2_router_table2" {
  subnet_id      = aws_subnet.private_subnet2.id
  route_table_id = aws_route_table.route_table_private2.id
}
