#! /bin/bash
yum -y update
yum -y install docker
usermod -a -G docker ec2-user
systemctl enable docker.service
systemctl start docker.service
docker run -P -p 80:80 -d nginxdemos/hello
/bin/echo "Docker installed, nginx started" >> /tmp/user_data.txt