output "lb_dns_name" {
  description = "DNS name of LB"
  value       = aws_lb.alb_web_app.dns_name
}

output "bastion1_ip" {
  description = "public IP of bastion 1"
  value       = aws_instance.bastion1.public_ip
}

output "bastion2_ip" {
  description = "public IP of bastion 2"
  value       = aws_instance.bastion2.public_ip
}

output "worker1_ip" {
  description = "private IP of worker 1"
  value       = aws_instance.worker1.private_ip
}

output "worker2_ip" {
  description = "private IP of worker 2"
  value       = aws_instance.worker2.private_ip
}
