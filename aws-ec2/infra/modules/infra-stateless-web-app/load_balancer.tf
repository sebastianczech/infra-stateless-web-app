resource "aws_lb" "alb_web_app" {
  name               = "alb-web-app"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_sg.id]
  subnets            = [aws_subnet.public_subnet1.id, aws_subnet.public_subnet2.id]

  tags = {
    Environment = "alb_web_app"
  }
}

resource "aws_lb_target_group" "alb_web_app_tg" {
  name     = "alb-web-app-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.vpc_web_app.id
}

resource "aws_lb_listener" "alb_web_app_listener" {
  load_balancer_arn = aws_lb.alb_web_app.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb_web_app_tg.arn
  }
}

resource "aws_lb_target_group_attachment" "alb_web_app_tg_worker1" {
  target_group_arn = aws_lb_target_group.alb_web_app_tg.arn
  target_id        = aws_instance.worker1.id
  port             = 80
}

resource "aws_lb_target_group_attachment" "alb_web_app_tg_worker2" {
  target_group_arn = aws_lb_target_group.alb_web_app_tg.arn
  target_id        = aws_instance.worker2.id
  port             = 80
}
