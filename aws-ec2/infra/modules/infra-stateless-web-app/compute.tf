resource "aws_key_pair" "infra_web_app_ssh_key" {
  key_name   = "infra_web_app_ssh_key"
  public_key = var.id_rsa_pub
}

resource "aws_instance" "bastion1" {
  ami                         = var.image_id
  instance_type               = var.image_size
  key_name                    = "infra_web_app_ssh_key"
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.public_subnet1.id
  vpc_security_group_ids      = [aws_security_group.bastion_sg.id]

  credit_specification {
    cpu_credits = "standard"
  }

  tags = {
    Name = "bastion1"
  }
}

resource "aws_instance" "worker1" {
  ami                         = var.image_id
  instance_type               = var.image_size
  key_name                    = "infra_web_app_ssh_key"
  associate_public_ip_address = false
  subnet_id                   = aws_subnet.private_subnet1.id
  vpc_security_group_ids      = [aws_security_group.worker_sg.id]
  user_data                   = file("${path.module}/install_docker.sh")

  depends_on = [aws_nat_gateway.gw_nat1]

  credit_specification {
    cpu_credits = "standard"
  }

  tags = {
    Name = "worker1"
  }
}

resource "aws_instance" "bastion2" {
  ami                         = var.image_id
  instance_type               = var.image_size
  key_name                    = "infra_web_app_ssh_key"
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.public_subnet2.id
  vpc_security_group_ids      = [aws_security_group.bastion_sg.id]

  credit_specification {
    cpu_credits = "standard"
  }

  tags = {
    Name = "bastion2"
  }
}

resource "aws_instance" "worker2" {
  ami                         = var.image_id
  instance_type               = var.image_size
  key_name                    = "infra_web_app_ssh_key"
  associate_public_ip_address = false
  subnet_id                   = aws_subnet.private_subnet2.id
  vpc_security_group_ids      = [aws_security_group.worker_sg.id]
  user_data                   = file("${path.module}/install_docker.sh")

  depends_on = [aws_nat_gateway.gw_nat2]

  credit_specification {
    cpu_credits = "standard"
  }

  tags = {
    Name = "worker2"
  }
}