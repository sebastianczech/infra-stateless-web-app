# network

variable "vpc_cird_block" {
  type    = string
  default = "10.0.0.0/16"
}

variable "subnet_cird_block_public1" {
  type    = string
  default = "10.0.1.0/24"
}

variable "subnet_cird_block_public2" {
  type    = string
  default = "10.0.2.0/24"
}

variable "subnet_cird_block_private1" {
  type    = string
  default = "10.0.3.0/24"
}

variable "subnet_cird_block_private2" {
  type    = string
  default = "10.0.4.0/24"
}

variable "region" {
  type    = string
  default = "us-east-1"
}

variable "az1" {
  type    = string
  default = "us-east-1a"
}

variable "az2" {
  type    = string
  default = "us-east-1b"
}

variable "my_public_ip" {
  type = string
}

# compute

variable "image_id" {
  type    = string
  default = "ami-0cff7528ff583bf9a"
}

variable "image_size" {
  type    = string
  default = "t2.micro"
}

variable "id_rsa_pub" {
  type = string
}
