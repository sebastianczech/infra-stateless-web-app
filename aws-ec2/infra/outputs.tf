output "lb_dns_name" {
  description = "DNS name of LB"
  value       = module.infra_stateless_web_app.lb_dns_name
}

output "bastion1_ip" {
  description = "public IP of bastion 1"
  value       = module.infra_stateless_web_app.bastion1_ip
}

output "bastion2_ip" {
  description = "public IP of bastion 2"
  value       = module.infra_stateless_web_app.bastion2_ip
}

output "worker1_ip" {
  description = "private IP of worker 1"
  value       = module.infra_stateless_web_app.worker1_ip
}

output "worker2_ip" {
  description = "private IP of worker 2"
  value       = module.infra_stateless_web_app.worker2_ip
}
