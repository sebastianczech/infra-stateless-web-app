terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region = var.region
}

module "infra_stateless_web_app" {
  source = "./modules/infra-stateless-web-app"

  vpc_cird_block             = var.vpc_cird_block
  subnet_cird_block_public1  = var.subnet_cird_block_public1
  subnet_cird_block_public2  = var.subnet_cird_block_public2
  subnet_cird_block_private1 = var.subnet_cird_block_private1
  subnet_cird_block_private2 = var.subnet_cird_block_private2
  region                     = var.region
  az1                        = var.az1
  az2                        = var.az2
  my_public_ip               = var.my_public_ip
  image_id                   = var.image_id
  image_size                 = var.image_size
  id_rsa_pub                 = var.id_rsa_pub
}

