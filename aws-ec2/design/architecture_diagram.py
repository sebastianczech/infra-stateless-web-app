from diagrams import Diagram, Cluster
from diagrams.aws.compute import EC2
from diagrams.aws.database import RDS
from diagrams.aws.network import ELB, VPC, InternetGateway, ALB, NATGateway
from diagrams.generic.device import Tablet
from diagrams.aws.compute import EC2AutoScaling
from diagrams.generic.os import LinuxGeneral
from diagrams.onprem.network import Nginx
from diagrams.generic.place import Datacenter

with Diagram("Architecture diagram", show=False, direction="TB"):
    inet = Datacenter("internet (HTTP/HTTPs)")
    user = Tablet("end user's web browser")
    
    ssh = LinuxGeneral("management (SSH)")
    
    with Cluster("VPC"):
        ig = InternetGateway("internet gateway")
        alb = ALB("application load balancer")
        # asg = EC2AutoScaling("auto scalling groups")
        with Cluster("Availability Zone 1"):
            with Cluster("Public subnet 1"):
                bastion1 = EC2("bastion host 1")
                nat1 = NATGateway("NAT gateway 1")
            with Cluster("Private subnet 1"):
                worker1 = EC2("worker 1")
                nginx1 = Nginx("nginx 1")
        with Cluster("Availability Zone 2"):
            with Cluster("Public subnet 2"):
                bastion2 = EC2("bastion host 2")
                nat2 = NATGateway("NAT gateway 2")
            with Cluster("Private subnet 2"):
                worker2 = EC2("worker 2")
                nginx2 = Nginx("nginx 2")    

        with Cluster("Autoscalling group"):     
            worker1
            worker2
    
    worker1 >> nginx1 
    worker2 >> nginx2
    bastion1 >> worker1
    bastion2 >> worker2
    # inet >> ig >> alb >> asg >> [worker1,worker2]
    user >> alb >> [worker1,worker2]
    bastion1 >> ig >> inet
    bastion2 >> ig >> inet
    worker1 >> nat1 >> inet
    worker2 >> nat2 >> inet
    ssh >> [bastion1,bastion2]
