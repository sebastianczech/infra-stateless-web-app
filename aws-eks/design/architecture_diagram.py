from diagrams import Diagram, Cluster
from diagrams.aws.compute import EC2, EKS
from diagrams.aws.database import RDS
from diagrams.aws.network import ELB, VPC, InternetGateway, ALB, NATGateway
from diagrams.generic.device import Tablet
from diagrams.aws.compute import EC2AutoScaling
from diagrams.generic.os import LinuxGeneral
from diagrams.onprem.network import Nginx
from diagrams.generic.place import Datacenter
from diagrams.k8s.compute import Deployment, Pod, ReplicaSet
from diagrams.k8s.network import Ingress, Service

with Diagram("Architecture diagram", show=False, direction="TB"):
    inet = Datacenter("internet (HTTP/HTTPs)")
    user = Tablet("end user's web browser")
    
    with Cluster("VPC"):
        elb = ELB("classic load balancer")
        eks = EKS("Kubernetes cluster")
        # asg = EC2AutoScaling("auto scalling groups")
        with Cluster("Availability Zone 1"):
            with Cluster("Public subnet 1"):
                nat1 = NATGateway("NAT gateway ")
            with Cluster("Private subnet 1"):
                worker1 = EC2("worker 1")
        with Cluster("Availability Zone 2"):
            with Cluster("Private subnet 2"):
                worker2 = EC2("worker 2")
        with Cluster("Availability Zone 3"):
            with Cluster("Private subnet 3"):
                worker3 = EC2("worker 3")

        with Cluster("Autoscalling group"):     
            worker1
            worker2
            worker3

        with Cluster("Namespace"):
            ing = Ingress("LB DNS NAME")
            nmsp = ing >> Service("svc")
            nmsp >> [Pod("nginx demos 1"),
                    Pod("nginx demos 2"), 
                    Pod("nginx demos 3")] << ReplicaSet("rs") << Deployment("dp")

        elb >> ing
        eks >> nmsp
        eks >> [worker1, worker2, worker3]
        user >> elb >> [worker1, worker2, worker3]
        [worker1, worker2, worker3] >> nat1 >> inet
    

