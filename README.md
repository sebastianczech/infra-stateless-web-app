# Infrastructure deployment to host a stateless web application

## Requirements

- Create the terraform deployment to host a stateless application containerised here: https://hub.docker.com/r/nginxdemos/hello/
- Create the architecture diagram for the deployment using: https://github.com/mingrammer/diagrams
- Ensure application is deployed behind a load balancer.
- On the point of the load balancer the application needs to be HA.
- Ensure that the network hosting of the web application is secured.
- If application is hosted in a VM build as well the infrastructure for safely access It via ssh
- Make use of service accounts where applicable.
- Depends on how your application is hosted, propose options for securing access to the web application itself.
- Create all the public/private networks needed to secure unwanted access from Internet to the infrastructure hosting the web application.
- Use a repository on www.gitlab.com to manage all the code part of the assignment and provide instructions on how to consume the repository.  
- Note: No restriction on public cloud used nor approach to host the web application.

## How to use repository

- Clone repository e.g using command ``git clone https://gitlab.com/sebastianczech/infra-stateless-web-app``
- Follow steps described for choosen solution

## Why 3 solutions were provided ?

Solution 1 with AWS & EC2 was prepared from scratch and it's based on virtual machines, on which simple Docker command was executed, which starts Ngninx demo. It's main solution, which was created to fulfill all requirements in easiest way (for me).

Solution 2 was based on [code from HashiCorp learn tutorial to provision an EKS cluster](https://learn.hashicorp.com/tutorials/terraform/eks), so it's not prepared from scratch. Internally code is using [code to provision EKS cluster](https://github.com/hashicorp/learn-terraform-provision-eks-cluster) with [module to create an Elastic Kubernetes (EKS)](https://registry.terraform.io/modules/terraform-aws-modules/eks/aws/18.26.3) and [module which creates VPC resources on AWS](https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/3.14.2). Besides that it was added Helm chart, which configures Nginx demo application. It's not main solution, but it was added to present different approach to fulfill requirementw. Because it's not main solution, I allowed myself to experiments with modules, which are publicly available and were prepared by community.

Solution 3 was prepared using similar approach as in solution 2 (also using Kubernetes cluster), but it was prepared in different way - all code was prepared by me from scratch while working on [free Kubernetes cluster on public cloud](https://github.com/sebastianczech/k8s-oci). Modules to [provision infrastructure for K8s](https://registry.terraform.io/modules/sebastianczech/infra-k8s-oracle-cloud/oci/latest) and [configure K8s cluster](https://registry.terraform.io/modules/sebastianczech/conf-k8s-oracle-cloud/oci/latest) were also published in Terraform registry on my private account. Using that code prepared by myself earlier, I adjusted it to fullfill requirements and use different cloud. In proposed solution every worker in Kubernetes cluster contains public and private IPs, because in Oracle always free option it's possible to have 4 instance of virtual machines - purposely I wanted to maximazie capacity of K8s cluster and I didn't want to use one of the machines as bastion host or machine, on which I will only execute Ansible playbook to configure K8s cluster.

## Solution 1 - AWS & EC2

### Architecture diagram

![Architecture diagram](aws-ec2/design/architecture_diagram.png)

### Prerequisites

Register and activate account in [AWS](https://console.aws.amazon.com/). After that install prerequisites on local machine:
 1. [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli)
 2. [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)

### Generate architecture diagram

If you want to regenerate architecture diagram from code, use below commands:

```
cd aws-ec2/design

python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt

python architecture_diagram.py
```

### Provision infrastructure

Whole infrastructure code can be deployed using below commands. Remember to pass correct AWS access key and secret for your account. After deployment in output from Terraform you can find all details how to access ALB or bastion hosts:

```
cd aws-ec2/infra

aws configure

terraform init
terraform plan
terraform apply -auto-approve
terraform output
```

If you want directly check, if application is accessible through ALB, use below commands:

```
export LB_DNS_NAME=`terraform output | grep lb_dns_name | awk -F\" '{print $2}'`
curl http://$LB_DNS_NAME
```

Using ALB DNS name you can also check application in web browser and you should receive below output:

![Screenshot 1](aws-ec2/app/screenshot1.png)

After refreshing page and next check address should be different:

![Screenshot 2](aws-ec2/app/screenshot2.png)

## Solution 2 - AWS & EKS

### Architecture diagram

![Architecture diagram](aws-eks/design/architecture_diagram.png)

### Prerequisites

Register and activate account in [AWS](https://console.aws.amazon.com/). After that install prerequisites on local machine:
 1. [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli)
 2. [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
 3. [aws-iam-authenticator](https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html)

### Provision infrastructure

Whole infrastructure code can be deployed using below commands. Remember to pass correct AWS access key and secret for your account. After deployment in output from Terraform you can find all details about Kubernetes cluster:

```
cd aws-eks/infra

aws configure

terraform init
terraform plan
terraform apply -auto-approve
terraform output
```

In order to access Kubernetes cluster from ``kubectl``, you can use below command:

```
terraform output -raw kubectl_config > ~/.kube/aws_eks.conf
export KUBECONFIG=$KUBECONFIG:~/.kube/config:~/.kube/aws_eks.conf
kubectl get all --all-namespaces
```

### Deploy stateless web application

On running infrastructure with Kubernetes cluster, application with nginx demo can be installed e.g. using Helm:

```
cd aws-eks/app

helm upgrade --install --atomic \
      --create-namespace --namespace nginxdemos \
      -f nginxdemos/values.yaml \
      nginxdemos nginxdemos
```

Nodes and all object in new namespaces can be checked by commands:

```
> kubectl get nodes -o wide
NAME                                       STATUS   ROLES    AGE   VERSION                INTERNAL-IP   EXTERNAL-IP   OS-IMAGE         KERNEL-VERSION                 CONTAINER-RUNTIME
ip-10-0-2-37.us-east-2.compute.internal    Ready    <none>   47m   v1.20.15-eks-99076b2   10.0.2.37     <none>        Amazon Linux 2   5.4.196-108.356.amzn2.x86_64   docker://20.10.13
ip-10-0-3-195.us-east-2.compute.internal   Ready    <none>   46m   v1.20.15-eks-99076b2   10.0.3.195    <none>        Amazon Linux 2   5.4.196-108.356.amzn2.x86_64   docker://20.10.13
ip-10-0-3-98.us-east-2.compute.internal    Ready    <none>   47m   v1.20.15-eks-99076b2   10.0.3.98     <none>        Amazon Linux 2   5.4.196-108.356.amzn2.x86_64   docker://20.10.13

> kubectl get all -n nginxdemos -o wide
NAME                                    READY   STATUS    RESTARTS   AGE     IP           NODE                                       NOMINATED NODE   READINESS GATES
pod/nginxdemos-chart-7dbf4fdd84-ccpqn   1/1     Running   0          3m19s   10.0.3.188   ip-10-0-3-98.us-east-2.compute.internal    <none>           <none>
pod/nginxdemos-chart-7dbf4fdd84-gwcjs   1/1     Running   0          31m     10.0.3.129   ip-10-0-3-195.us-east-2.compute.internal   <none>           <none>
pod/nginxdemos-chart-7dbf4fdd84-txs6r   1/1     Running   0          31m     10.0.2.150   ip-10-0-2-37.us-east-2.compute.internal    <none>           <none>

NAME                       TYPE           CLUSTER-IP       EXTERNAL-IP                                                               PORT(S)        AGE   SELECTOR
service/nginxdemos-chart   LoadBalancer   172.20.239.148   a322a60c74f9e44b484aeed106d98b81-1280824852.us-east-2.elb.amazonaws.com   80:30146/TCP   31m   app.kubernetes.io/instance=nginxdemos,app.kubernetes.io/name=nginxdemos

NAME                               READY   UP-TO-DATE   AVAILABLE   AGE   CONTAINERS   IMAGES                 SELECTOR
deployment.apps/nginxdemos-chart   3/3     3            3           31m   nginxdemos   nginxdemos/hello:0.3   app.kubernetes.io/instance=nginxdemos,app.kubernetes.io/name=nginxdemos

NAME                                          DESIRED   CURRENT   READY   AGE   CONTAINERS   IMAGES                 SELECTOR
replicaset.apps/nginxdemos-chart-7dbf4fdd84   3         3         3       31m   nginxdemos   nginxdemos/hello:0.3   app.kubernetes.io/instance=nginxdemos,app.kubernetes.io/name=nginxdemos,pod-template-hash=7dbf4fdd84
```

Using load balancer public DNS name, you can check application in web browser:

![Screenshot 1](oci-k8s/app/screenshot1.png)

After refreshing page and for next check most probably you should received different output:

![Screenshot 2](oci-k8s/app/screenshot2.png)

## Solution 3 - OCI & K8S

### Architecture diagram

![Architecture diagram](oci-k8s/design/architecture_diagram.png)

### Prerequisites

Register and activate account in [Oracle Cloud](https://cloud.oracle.com/). After that install prerequisites on local machine:
 1. [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli)
 2. [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
 3. [Helm](https://helm.sh/docs/intro/install/)
 4. [OCI](https://docs.oracle.com/en-us/iaas/Content/API/SDKDocs/cliinstall.htm)

### Generate architecture diagram

If you want to regenerate architecture diagram from code, use below commands:

```
cd oci-k8s/design

python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt

python architecture_diagram.py
```

### Provision infrastructure

Whole infrastructure code can be deployed using below commands. Remember to properly authenticate in Oracle cloud. After deployment in output from Terraform you can find all details how to access nodes for Kubernetes cluster:

```
cd oci-k8s/infra

oci session authenticate --region eu-frankfurt-1 --profile-name k8s-oci

terraform init
terraform plan
terraform apply -auto-approve
terraform output
```

### Configure Kubernetes cluster

After whole infrastructure is provisioned, on deployed nodes Kubernetes cluster can be setup using below commands and Ansible playbooks:

```
cd oci-k8s/conf

python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt

ansible-playbook -i ../infra/inventory.ini playbook.yml
```

In order to access Kubernetes cluster using e.g. ``kubectl`` command, you can use below steps:

```
export KUBECONFIG=$KUBECONFIG:~/.kube/config:~/.kube/microk8s.conf
kubectl get all --all-namespaces
```

### Deploy stateless web application

On running infrastructure with Kubernetes cluster, application with nginx demo can be installed e.g. using Helm:

```
cd oci-k8s/app

helm upgrade --install --atomic \
      --create-namespace --namespace nginxdemos \
      -f nginxdemos/values.yaml \
      nginxdemos nginxdemos
```

You can check final results by running commands:

```
cd ../infra

export LB_PUBLIC_IP=`terraform output | grep lb_public_ip | awk -F\" '{print $2}'`
curl http://$LB_PUBLIC_IP
```

Below you can find some results of ``kubectl`` after deploying nginx demo:

```
> kubectl get nodes -o wide
NAME        STATUS   ROLES    AGE   VERSION                    INTERNAL-IP    EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION       CONTAINER-RUNTIME
k8s-node2   Ready    <none>   56m   v1.23.6-2+d01e7eb5fe4e94   172.16.0.212   <none>        Ubuntu 20.04.4 LTS   5.13.0-1018-oracle   containerd://1.5.11
k8s-node3   Ready    <none>   55m   v1.23.6-2+d01e7eb5fe4e94   172.16.0.35    <none>        Ubuntu 20.04.4 LTS   5.13.0-1018-oracle   containerd://1.5.11
k8s-node1   Ready    <none>   57m   v1.23.6-2+d01e7eb5fe4e94   172.16.0.186   <none>        Ubuntu 20.04.4 LTS   5.13.0-1018-oracle   containerd://1.5.11
k8s-node0   Ready    <none>   61m   v1.23.6-2+d01e7eb5fe4e94   172.16.0.124   <none>        Ubuntu 20.04.4 LTS   5.13.0-1018-oracle   containerd://1.5.11

> kubectl get all -n nginxdemos -o wide
NAME                                   READY   STATUS    RESTARTS   AGE   IP             NODE        NOMINATED NODE   READINESS GATES
pod/nginxdemos-chart-98446645d-whnlt   1/1     Running   0          15m   10.1.155.131   k8s-node0   <none>           <none>
pod/nginxdemos-chart-98446645d-g552f   1/1     Running   0          15m   10.1.36.67     k8s-node1   <none>           <none>

NAME                       TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE   SELECTOR
service/nginxdemos-chart   NodePort   10.152.183.85   <none>        80:30260/TCP   15m   app.kubernetes.io/instance=nginxdemos,app.kubernetes.io/name=nginxdemos

NAME                               READY   UP-TO-DATE   AVAILABLE   AGE   CONTAINERS   IMAGES                 SELECTOR
deployment.apps/nginxdemos-chart   2/2     2            2           15m   nginxdemos   nginxdemos/hello:0.3   app.kubernetes.io/instance=nginxdemos,app.kubernetes.io/name=nginxdemos

NAME                                         DESIRED   CURRENT   READY   AGE   CONTAINERS   IMAGES                 SELECTOR
replicaset.apps/nginxdemos-chart-98446645d   2         2         2       15m   nginxdemos   nginxdemos/hello:0.3   app.kubernetes.io/instance=nginxdemos,app.kubernetes.io/name=nginxdemos,pod-template-hash=98446645d
```

Using load balancer public IP, you can check application in web browser:

![Screenshot 1](oci-k8s/app/screenshot1.png)

After refreshing page and for next check most probably you should received different output:

![Screenshot 2](oci-k8s/app/screenshot2.png)