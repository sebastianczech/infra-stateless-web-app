from diagrams import Diagram, Cluster
from diagrams.oci.network import Vcn
from diagrams.oci.compute import VM
from diagrams.oci.network import InternetGateway, LoadBalancer, Vcn, RouteTable
from diagrams.k8s.compute import Deployment, Pod, ReplicaSet
from diagrams.k8s.network import Ingress, Service
from diagrams.generic.device import Tablet

with Diagram("Architecture diagram", show=False):
    
    inet = Tablet("Internet")

    with Cluster("Oracle Cloud Region"):        
        with Cluster("Networking"):
            vcn = Vcn("K8s VCN")
            rt = RouteTable("K8s default route table")
            ig = InternetGateway("K8s inet gateway")
            lb = LoadBalancer("K8s load balancer")            

        with Cluster("Compute"):
            k8s_nodes = [
                VM("k8s node 0"),
                VM("k8s node 1"),
                VM("k8s node 2"),
                VM("k8s node 3")
            ]

        with Cluster("Namespace"):
            nmsp = Ingress("LB PUBLIC IP") >> Service("svc")
            nmsp >> [Pod("nginx demos 1"),
                    Pod("nginx demos 2")] << ReplicaSet("rs") << Deployment("dp")

    inet >> ig >> vcn >> lb >> k8s_nodes >> nmsp
    vcn >> rt >> ig